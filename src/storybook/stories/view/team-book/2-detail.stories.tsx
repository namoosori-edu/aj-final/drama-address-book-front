import * as React from 'react';
import { TeamAddressPageDetail } from '../../../../comp';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { teamPageId } from '../../data/testData';

export default {
  title: 'view/address/team-book',
  component: TeamAddressPageDetail,
} as ComponentMeta<typeof TeamAddressPageDetail>;

const Template: ComponentStory<typeof TeamAddressPageDetail> = () => {
  //
  return (
    <TeamAddressPageDetail
      addressPageId={teamPageId}
    />
  );
};

export const detail = Template.bind({});
detail.args = {
  //
};
