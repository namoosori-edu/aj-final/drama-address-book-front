import { TeamAddressPageForm } from '../../../../comp';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';

export default {
  title: 'view/address/team-book',
  component: TeamAddressPageForm,
} as ComponentMeta<typeof TeamAddressPageForm>;

const Registration: ComponentStory<typeof TeamAddressPageForm> = () => {
  //
  return (
    <TeamAddressPageForm
      addressBookId='1@1:1:1'
    />
  );
};

export const registration = Registration.bind({});
registration.args = {
  //
};
