import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { TeamAddressPageList } from '../../../../comp';

export default {
  title: 'view/address/team-book',
  component: TeamAddressPageList,
} as ComponentMeta<typeof TeamAddressPageList>;

const Template: ComponentStory<typeof TeamAddressPageList> = () => {
  //
  return (
    <TeamAddressPageList
      addressBookId="1@1:1:1"
    >
      <TeamAddressPageList.Sort/>
      <TeamAddressPageList.Content/>
    </TeamAddressPageList>
  );
};

export const list = Template.bind( {});
list.args = {
  //
};
