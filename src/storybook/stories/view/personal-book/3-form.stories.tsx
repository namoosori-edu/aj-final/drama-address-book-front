import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { PersonalAddressPageForm } from '../../../../comp';
import { personalPageId } from '../../data/testData';

export default {
  title: 'view/address/personal-book',
  component: PersonalAddressPageForm,
} as ComponentMeta<typeof PersonalAddressPageForm>;

const Registration: ComponentStory<typeof PersonalAddressPageForm> = () => {
  //
  return (
    <PersonalAddressPageForm
      addressBookId="1@1:1:1"
    />
  );
};

export const registration = Registration.bind({});
registration.args = {
//
};


const Modification: ComponentStory<typeof PersonalAddressPageForm> = () => {
  //
  return (
    <PersonalAddressPageForm
      addressPageId={personalPageId}
    />
  );
};

export const modification = Modification.bind({});
modification.args = {
//
};

