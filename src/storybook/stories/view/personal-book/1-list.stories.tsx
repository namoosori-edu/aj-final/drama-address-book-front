import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { PersonalAddressPageList } from '../../../../comp';

export default {
  title: 'view/address/personal-book',
  component: PersonalAddressPageList,
} as ComponentMeta<typeof PersonalAddressPageList>;

const Template: ComponentStory<typeof PersonalAddressPageList> = () => {
  //
  return (
    <PersonalAddressPageList
      addressBookId="1@1:1:1"
    >
      <PersonalAddressPageList.Sort />
      <PersonalAddressPageList.Content />
    </PersonalAddressPageList>
  );
};

export const list = Template.bind({});
list.args = {
//
};
