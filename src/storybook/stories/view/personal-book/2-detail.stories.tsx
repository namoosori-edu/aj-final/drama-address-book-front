
import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { PersonalAddressPageDetail } from '../../../../comp';
import { personalPageId } from '../../data/testData';

export default {
  title: 'view/address/personal-book',
  component: PersonalAddressPageDetail,
} as ComponentMeta<typeof PersonalAddressPageDetail>;

const Template: ComponentStory<typeof PersonalAddressPageDetail> = () => {
  //
  return (
    <PersonalAddressPageDetail
      addressPageId={personalPageId}
    />
  );
};

export const detail = Template.bind({});
detail.args = {
//
};
