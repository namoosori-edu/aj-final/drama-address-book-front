import React, {FC, useEffect} from 'react';
import {observer, useLocalObservable} from 'mobx-react';
import {AddressPage} from '../../../api';
import {PersonalPageStateKeeper} from '../../../state';
import AddressPageFormView from './view/AddressPageFormView';
import AddressPageButtonView from './view/AddressPageButtonView';
import {DramaException} from '@nara/accent';

const PersonalPageFormContainer = observer(
  ({
     addressBookId,
     addressPageId,
     onClickCancel = () => undefined,
     onSuccess = () => undefined,
     onFail = () => undefined,
   }: {
    addressBookId?: string;
    addressPageId?: string;
    onClickCancel?: () => void;
    onSuccess?: () => void;
    onFail?: () => void;
  }) => {
    //
    const addressPageStateKeeper = useLocalObservable(() => PersonalPageStateKeeper.instance);
    const {addressPage} = addressPageStateKeeper;

    useEffect(() => {
      init();
    }, []);

    const init = () => {
      if (addressPageId) {
        addressPageStateKeeper.findPersonalPageById(addressPageId);
      } else if (addressBookId) {
        addressPageStateKeeper.init(addressBookId);
      }
    };

    const handleClickDefault = async () => {
      //
      if (!addressPage) {
        throw new DramaException('AddressPageDetail', 'addressPage should not be null.');
      }

      await addressPageStateKeeper.togglePersonalBaseAddress(addressPage);
      await init();
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      //
      const {name, value} = event.target;

      addressPageStateKeeper.setAddressPageProp(name, value);
    };

    const handleClickAddField = () => {
      //
      addressPageStateKeeper.addField();
    };

    const handleClickRemoveField = (index: number) => () => {
      //
      addressPageStateKeeper.removeField(index);
    };

    const handleChangeField = (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
      //
      const {name, value} = event.target;

      addressPageStateKeeper.setAddressPageField(index, name, value);
    };

    const handleClickSave = async () => {
      //
      if (!addressPage) {
        throw new DramaException('AddressPageForm', 'addressPage should not be null.');
      }

      const response = addressPageId ?
        await addressPageStateKeeper.modify(addressPageId, AddressPage.asNameValues(addressPage))
        : await addressPageStateKeeper.register(addressPage);

      if (response.failed) {
        onFail();
      } else {
        onSuccess();
      }
    };

    return addressPage && (
      <>
        <AddressPageFormView
          addressPage={addressPage}
          onClickDefault={handleClickDefault}
          onChange={handleChange}
          onChangeField={handleChangeField}
          onClickAddField={handleClickAddField}
          onClickRemoveField={handleClickRemoveField}
        />
        <AddressPageButtonView
          onClickSave={handleClickSave}
          onClickCancel={onClickCancel}
        />
      </>
    );
  });

export default PersonalPageFormContainer;
