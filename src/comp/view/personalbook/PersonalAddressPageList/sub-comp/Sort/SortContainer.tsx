import React, {FC} from 'react';
import {observer, useLocalObservable} from 'mobx-react';
import {Box, Button, Divider} from '@mui/material';
import {AddressPage} from '../../../../../api';
import {PersonalPagesStateKeeper} from '../../../../../state';


const SortContainer = observer(
  ({}) => {
    //
    const addressPagesStateKeeper = useLocalObservable(() => PersonalPagesStateKeeper.instance);
    const {addressBookId} = addressPagesStateKeeper;

    const handleChangeSortingField = (name: keyof AddressPage) => () => {
      //
      addressPagesStateKeeper.setOffsetProp('sortingField', name);
      addressPagesStateKeeper.findPersonalAddressPages(addressBookId);
    };

    return (
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          width: 'fit-content',
          color: 'text.secondary',
        }}
      >
        <Button
          color="inherit"
          onClick={handleChangeSortingField('registrationTime')}
        >
          등록일순
        </Button>
        <Divider orientation="vertical" variant="middle" flexItem/>
        <Button
          color="inherit"
          onClick={handleChangeSortingField('name')}
        >
          이름순
        </Button>
      </Box>
    );
  });

export default SortContainer;
