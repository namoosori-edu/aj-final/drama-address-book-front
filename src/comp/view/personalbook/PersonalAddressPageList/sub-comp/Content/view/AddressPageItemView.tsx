import React, {FC, useCallback} from 'react';
import {observer} from 'mobx-react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import {ContentPaste, LocationOn, PhoneAndroid} from '@mui/icons-material';
import {AddressPage} from '../../../../../../api';
import {StarButton, TextAvatar} from "../../../../../shared";

const AddressPageItemView = observer(
  ({
     addressPage,
     onClickDefault,
     onClickAddressPage,
   }: {
    addressPage: AddressPage;
    onClickDefault: (addressPage: AddressPage) => void;
    onClickAddressPage: (addressPage: AddressPage) => void;
  }) => {
    const defaultText = '-';

    const getTruncatedText = (text: string | null | undefined) => {
      if (!text) {
        return defaultText;
      }

      const maxTextLength = 15;
      const formattedText = text.length > maxTextLength ? `${text.substring(0, maxTextLength)}...` : text;

      return formattedText || defaultText;
    };

    const handleClickAddressPage = useCallback(
      (addressPage: AddressPage) => () => onClickAddressPage(addressPage),
      [addressPage],
    );

    const handleClickDefault = useCallback(
      (addressPage: AddressPage) => () => onClickDefault(addressPage),
      [addressPage],
    );

    return (
      <Card sx={{maxWidth: 359}} elevation={3} onClick={handleClickAddressPage(addressPage)}>
        <CardActionArea sx={{p: 1}}>
          <CardHeader
            avatar={<TextAvatar text={addressPage.name}/>}
            action={
              <StarButton
                baseAddress={addressPage.baseAddress}
                onClick={handleClickDefault(addressPage)}
              />
            }
            title={addressPage.name}
            titleTypographyProps={{variant: 'h6'}}
          />
          <CardContent sx={{height: 120, overflow: 'auto'}}>
            <List>
              <ListItem disablePadding>
                <ListItemIcon>
                  <LocationOn sx={{color: 'lightgrey'}}/>
                </ListItemIcon>
                <ListItemText
                  primary={addressPage.address?.fullAddress}
                  primaryTypographyProps={{variant: 'body2', sx: {color: 'gray'}}}
                />
              </ListItem>
              <ListItem disablePadding>
                <ListItemIcon>
                  <PhoneAndroid sx={{color: 'lightgrey'}}/>
                </ListItemIcon>
                <ListItemText
                  primary={getTruncatedText(addressPage.phoneNumber)}
                  primaryTypographyProps={{variant: 'body2', sx: {color: 'gray'}}}
                />
              </ListItem>
              <ListItem disablePadding>
                <ListItemIcon>
                  <ContentPaste sx={{color: 'lightgrey'}}/>
                </ListItemIcon>
                <ListItemText
                  primary={getTruncatedText(addressPage.memo)}
                  primaryTypographyProps={{variant: 'body2', sx: {color: 'gray'}}}
                />
              </ListItem>
            </List>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  });

export default AddressPageItemView;
