
import PersonalAddressPageListContainer from './PersonalAddressPageListContainer';
import Content from './sub-comp/Content';
import Sort from './sub-comp/Sort';


type PersonalAddressPageListComponent = typeof PersonalAddressPageListContainer & {
  Content: typeof Content;
  Sort: typeof Sort;
};

const PersonalAddressPageList = PersonalAddressPageListContainer as PersonalAddressPageListComponent;

PersonalAddressPageList.Content = Content;
PersonalAddressPageList.Sort = Sort;

export { PersonalAddressPageList };
