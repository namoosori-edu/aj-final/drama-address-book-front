import React, {FC, useEffect} from 'react';
import {observer, useLocalObservable} from 'mobx-react';
import AddressPageDetailView from './view/AddressPageDetailView';
import {PersonalPageStateKeeper} from '../../../state';
import {DramaException} from '@nara/accent';

const PersonalAddressPageDetailContainer = observer(
  ({
     addressPageId,
   }: {
    addressPageId: string
  }) => {
    //
    const addressPageStateKeeper = useLocalObservable(() => PersonalPageStateKeeper.instance);
    const {addressPage} = addressPageStateKeeper;

    useEffect(() => {
      init();
    }, []);

    const init = () => {
      addressPageStateKeeper.findPersonalPageById(addressPageId);
    };

    const handleClickDefault = async () => {
      //
      if (!addressPage) {
        throw new DramaException('AddressPageDetail', 'addressPage should not be null.');
      }

      await addressPageStateKeeper.togglePersonalBaseAddress(addressPage);
      init();
    };

    return addressPage && (
      <AddressPageDetailView
        addressPage={addressPage}
        onClickDefault={handleClickDefault}
      />
    );
  });

export default PersonalAddressPageDetailContainer;
