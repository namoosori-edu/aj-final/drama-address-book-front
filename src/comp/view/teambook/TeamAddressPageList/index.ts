import TeamAddressPageListContainer from "./TeamAddressPageListContainer";
import Content from './sub-comp/Content';
import Sort from './sub-comp/Sort';


type TeamAddressPageListComponent = typeof TeamAddressPageListContainer & {
  Content: typeof Content;
  Sort: typeof Sort;
};

const TeamAddressPageList = TeamAddressPageListContainer as TeamAddressPageListComponent;

TeamAddressPageList.Content = Content;
TeamAddressPageList.Sort = Sort;

export { TeamAddressPageList };
