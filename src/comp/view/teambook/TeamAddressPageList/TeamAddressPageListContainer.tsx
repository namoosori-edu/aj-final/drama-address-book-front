import {observer, useLocalObservable} from "mobx-react";
import React, {PropsWithChildren} from "react";
import {FC, ReactNode, useEffect} from "react";
import {TeamPagesStateKeeper, TeamPageStateKeeper} from "../../../state";

const TeamAddressPageListContainer = observer(
  ({
    addressBookId,
    children,
  }:{
    addressBookId: string;
    children: ReactNode;
  }) => {
    //
    const addressPagesStateKeeper = useLocalObservable(() => TeamPagesStateKeeper.instance);

    useEffect( () => {
      init();
    }, []);

    const init = () => {
        addressPagesStateKeeper.findTeamAddressPages(addressBookId);
    };

    return <>{children}</>;
  });

export default TeamAddressPageListContainer;
