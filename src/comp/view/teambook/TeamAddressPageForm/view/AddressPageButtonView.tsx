import {observer} from "mobx-react";
import {FC} from "react";
import {Button} from "@mui/material";
import React from "react";
import {ButtonGroupWrapper} from "../../../shared";

const AddressPageButtonView = observer(
  ({
     onClickSave,
     onClickCancel,
   }: {
    onClickSave: () => void;
    onClickCancel: () => void;

  }) => {
    return (
      <ButtonGroupWrapper>
        <Button size='large' variant='contained' onClick={onClickSave}>저장</Button>
        <Button size='large' variant='outlined' onClick={onClickCancel}>취소</Button>
      </ButtonGroupWrapper>
    );
  });

export default AddressPageButtonView;
