import {observer, useLocalObservable} from "mobx-react";
import React from "react";
import {FC, useEffect} from "react";
import {TeamPageStateKeeper} from "../../../state";
import AddressPageFormView from './view/AddressPageFormView';
import AddressPageButtonView from './view/AddressPageButtonView';
import {DramaException} from "@nara/accent";
import {AddressPage} from "../../../api";

const TeamPageFormContainer = observer(
  ({
     addressPageId,
     addressBookId,
     onClickCancel = () => undefined,
     onSuccess = () => undefined,
     onFail = () => undefined,
   }: {
    addressPageId?: string;
    addressBookId?: string;
    onClickCancel?: () => void;
    onSuccess?: () => void;
    onFail?: () => void;
  }) => {
    //
    const addressPageStateKeeper = useLocalObservable(() => TeamPageStateKeeper.instance);
    const {addressPage} = addressPageStateKeeper;

    useEffect(() => {
      init();
    }, []);

    const init = () => {
      if (addressPageId) {
        addressPageStateKeeper.findTeamPageById(addressPageId);
      } else if (addressBookId) {
        addressPageStateKeeper.init(addressBookId);
      }
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      //
      const {name, value} = event.target;

      addressPageStateKeeper.setAddressPageProp(name, value);
    }

    const handleClickAddField = () => {
      //
      addressPageStateKeeper.addField();
    }

    const handleClickRemoveField = (index: number) => {
      //
      addressPageStateKeeper.removeField(index);
    }

    const handleChangeField = (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
      //
      const {name, value} = event.target;

      addressPageStateKeeper.setAddressPageField(index, name, value);
    }

    const handleClickSave = async () => {
      //
      if (!addressPage) {
        throw new DramaException('AddressPageForm', 'addressPage should not be null.');
      }

      const response = addressPageId ?
        await addressPageStateKeeper.modify(addressPageId, AddressPage.asNameValues(addressPage))
        : await addressPageStateKeeper.register(addressPage);

      if (response.failed) {
        onFail();
      } else {
        onSuccess();
      }
    };

    return addressPage && (
      <>
        <AddressPageFormView
          addressPage={addressPage}
          onChange={handleChange}
          onChangeField={handleChangeField}
          onClickAddField={handleClickAddField}
          onClickRemoveField={handleClickRemoveField}
        />
        <AddressPageButtonView
          onClickSave={handleClickSave}
          onClickCancel={onClickCancel}
        />
      </>
    );
  });

export default TeamPageFormContainer;
