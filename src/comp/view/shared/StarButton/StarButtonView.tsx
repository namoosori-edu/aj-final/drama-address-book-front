import React, {FC} from 'react';
import {observer} from 'mobx-react';
import {IconButton, IconButtonProps} from '@mui/material';
import {Star, StarBorder} from '@mui/icons-material';


const StarButtonView = observer(
  ({
     baseAddress,
     ...iconButtonProps
   }: {
    baseAddress: boolean;
  } & IconButtonProps) => {
    return (
      <IconButton
        {...iconButtonProps}
        onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
          event.stopPropagation();
          if (iconButtonProps.onClick) {
            iconButtonProps.onClick(event);
          }
        }}
      >
        <Star style={{color: baseAddress ? 'orange' : 'lightgrey'}}/>
      </IconButton>
    );
  });

export default StarButtonView;
