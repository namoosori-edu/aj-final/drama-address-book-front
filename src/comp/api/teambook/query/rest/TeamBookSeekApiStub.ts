import { ApiClient } from '@nara/prologue';
import { AddressBook, AddressPage } from '../../../address';
import { FindTeamBookQuery, FindTeamPageQuery, FindTeamPagesPagedQuery, FindTeamPagesQuery } from '../query';
import {OffsetElementList} from "@nara/accent";


class TeamBookSeekApiStub {
  static _instance: TeamBookSeekApiStub;

  private readonly client = new ApiClient('/api/address-book/feature/teambook', {
    resDataName: 'queryResult',
  });

  static get instance() {
    if (!TeamBookSeekApiStub._instance) {
      TeamBookSeekApiStub._instance = new TeamBookSeekApiStub();
    }
    return TeamBookSeekApiStub._instance;
  }

  async findTeamBook(query: FindTeamBookQuery): Promise<AddressBook> {
    return this.client.postNotNull<AddressBook>(AddressBook, '/find-team-book/query', query);
  }

  async findTeamPages(query: FindTeamPagesQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(AddressPage, '/find-team-pages/query', query);
  }

  async findTeamPagesPaged(query: FindTeamPagesPagedQuery): Promise<OffsetElementList<AddressPage>> {
    return this.client.postOffsetElementList<AddressPage>(AddressPage, '/find-team-pages-paged/query', query);
  }

  async findTeamPage(query: FindTeamPageQuery): Promise<AddressPage> {
    return this.client.postNotNull<AddressPage>(AddressPage, '/find-team-page/query', query);
  }
}

export default TeamBookSeekApiStub;
