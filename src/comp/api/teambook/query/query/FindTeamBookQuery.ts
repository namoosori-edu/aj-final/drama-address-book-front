import { QueryRequest } from '@nara/accent';
import { AddressBook } from '../../../address';


class FindTeamBookQuery extends QueryRequest<AddressBook> {
  addressBookId: string;

  constructor(addressBookId: string) {
    super(AddressBook);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindTeamBookQuery): FindTeamBookQuery {
    const query = new FindTeamBookQuery(
      domain.addressBookId,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindTeamBookQuery(
      addressBookId,
    );

    return query;
  }

}

export default FindTeamBookQuery;
