import { QueryRequest } from '@nara/accent';
import { AddressPage } from '../../../address';


class FindTeamPagesQuery extends QueryRequest<AddressPage[]> {
  addressBookId: string;

  constructor(addressBookId: string) {
    super(AddressPage);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindTeamPagesQuery): FindTeamPagesQuery {
    const query = new FindTeamPagesQuery(
      domain.addressBookId,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindTeamPagesQuery(
      addressBookId,
    );

    return query;
  }

}

export default FindTeamPagesQuery;
