import { CommandRequest } from '@nara/accent';
import { AddressPageCdo } from '../../../address';


class RegisterTeamBookCommand extends CommandRequest {
  addressPageCdo: AddressPageCdo;

  constructor(addressPageCdo: AddressPageCdo) {
    super();
    this.addressPageCdo = addressPageCdo;
  }

  static new(addressPageCdo: AddressPageCdo) {
    const command = new RegisterTeamBookCommand(
      addressPageCdo,
    );

    return command;
  }

}

export default RegisterTeamBookCommand;
