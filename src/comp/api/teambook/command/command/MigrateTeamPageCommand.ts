import { CommandRequest } from '@nara/accent';


class MigrateTeamPageCommand extends CommandRequest {
  addressBookId: string;

  addressPageId: string;

  constructor(addressBookId: string, addressPageId: string) {
    // super(CommandType.User);
    super();
    this.addressBookId = addressBookId;
    this.addressPageId = addressPageId;
  }

  static new(addressBookId: string, addressPageId: string) {
    const command = new MigrateTeamPageCommand(
      addressBookId,
      addressPageId,
    );

    return command;
  }

}

export default MigrateTeamPageCommand;
