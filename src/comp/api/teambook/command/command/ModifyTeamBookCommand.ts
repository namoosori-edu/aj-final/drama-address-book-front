import { CommandRequest, NameValueList } from '@nara/accent';


class ModifyTeamBookCommand extends CommandRequest {
  addressPageId: string;

  nameValueList: NameValueList;

  constructor(addressPageId: string, nameValueList: NameValueList) {
    super();
    this.addressPageId = addressPageId;
    this.nameValueList = nameValueList;
  }

  static new(addressPageId: string, nameValueList: NameValueList) {
    const command = new ModifyTeamBookCommand(
      addressPageId,
      nameValueList,
    );

    return command;
  }

}

export default ModifyTeamBookCommand;
