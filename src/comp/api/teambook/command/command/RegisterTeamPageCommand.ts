import { CommandRequest } from '@nara/accent';
import { AddressPageCdo } from '../../../address';


class RegisterTeamPageCommand extends CommandRequest {
  addressPageCdo: AddressPageCdo;

  constructor(addressPageCdo: AddressPageCdo) {
    super();
    this.addressPageCdo = addressPageCdo;
  }

  static new(addressPageCdo: AddressPageCdo) {
    const command = new RegisterTeamPageCommand(
      addressPageCdo,
    );

    return command;
  }

}

export default RegisterTeamPageCommand;
