import { CommandRequest } from '@nara/accent';
import { AddressBookCdo } from '../../../address';


class RegisterPersonalBookCommand extends CommandRequest {
  addressBookCdo: AddressBookCdo;

  constructor(addressBookCdo: AddressBookCdo) {
    super();
    this.addressBookCdo = addressBookCdo;
  }

  static new(addressBookCdo: AddressBookCdo) {
    const command = new RegisterPersonalBookCommand(
      addressBookCdo,
    );

    return command;
  }

}

export default RegisterPersonalBookCommand;
