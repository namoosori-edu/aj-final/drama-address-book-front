import { CommandRequest } from '@nara/accent';
import { AddressPageCdo } from '../../../address';


class RegisterPersonalPageCommand extends CommandRequest {
  addressPageCdo: AddressPageCdo;

  constructor(addressPageCdo: AddressPageCdo) {
    super();
    this.addressPageCdo = addressPageCdo;
  }

  static new(addressPageCdo: AddressPageCdo) {
    const command = new RegisterPersonalPageCommand(
      addressPageCdo,
    );

    return command;
  }

}

export default RegisterPersonalPageCommand;
