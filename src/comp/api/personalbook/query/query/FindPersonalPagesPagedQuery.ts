import { QueryRequest, Offset } from '@nara/accent';
import { AddressPage } from '../../../address';


class FindPersonalPagesPagedQuery extends QueryRequest<AddressPage[]> {
  addressBookId: string;

  offset: Offset;

  constructor(addressBookId: string, offset: Offset) {
    super(AddressPage);
    this.addressBookId = addressBookId;
    this.offset = offset;
  }

  static fromDomain(domain: FindPersonalPagesPagedQuery): FindPersonalPagesPagedQuery {
    const query = new FindPersonalPagesPagedQuery(
      domain.addressBookId,
      domain.offset,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string, offset: Offset) {
    const query = new FindPersonalPagesPagedQuery(
      addressBookId,
      offset,
    );

    return query;
  }

}

export default FindPersonalPagesPagedQuery;
