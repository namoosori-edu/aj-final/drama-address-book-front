import { QueryRequest } from '@nara/accent';
import { AddressPage } from '../../../address';


class FindPersonalPagesQuery extends QueryRequest<AddressPage[]> {
  addressBookId: string;

  constructor(addressBookId: string) {
    super(AddressPage);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindPersonalPagesQuery): FindPersonalPagesQuery {
    const query = new FindPersonalPagesQuery(
      domain.addressBookId,

    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindPersonalPagesQuery(
      addressBookId,
    );

    return query;
  }

}

export default FindPersonalPagesQuery;
