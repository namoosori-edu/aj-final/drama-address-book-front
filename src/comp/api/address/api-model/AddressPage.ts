import { NameValueList, StageEntity } from '@nara/accent';
import { Address, GeoCoordinate, Field } from './vo';


class AddressPage extends StageEntity {
  geoCoordinate: GeoCoordinate | null = null;

  name: string;

  address: Address | null = null;

  phoneNumber: string;

  fields: Field[] = [];

  baseAddress: boolean;

  memo: string;

  addressBookId: string;

  constructor(name: string, phoneNumber: string, baseAddress: boolean, memo: string, addressBookId: string) {
    super();
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.baseAddress = baseAddress;
    this.memo = memo;
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: AddressPage): AddressPage {
    const addressPage = new AddressPage(
      domain.name,
      domain.phoneNumber,
      domain.baseAddress,
      domain.memo,
      domain.addressBookId,
    );

    addressPage.setStageEntity(domain);
    addressPage.geoCoordinate = domain.geoCoordinate ? GeoCoordinate.fromDomain(domain.geoCoordinate) : null;
    addressPage.address = domain.address ? Address.fromDomain(domain.address) : null;
    addressPage.fields = domain.fields ? Field.fromDomains(domain.fields) : [];
    return addressPage;
  }

  static fromDomains(domains: AddressPage[]): AddressPage[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static asNameValues(model: AddressPage): NameValueList {
    const addressPage = { ...model };

    addressPage.fields = addressPage.fields.filter(field => field.name || field.value);

    return NameValueList.fromModel(AddressPage, addressPage, {
      geoCoordinate: 'Json',
      name: String,
      address: 'Json',
      phoneNumber: String,
      fields: 'Json',
      baseAddress: String,
      memo: String,
    });
  }

  static new(addressBookId = ''): AddressPage {
    const addressPage = new AddressPage('', '', false, '', addressBookId);

    addressPage.address = Address.new();

    return addressPage;
  }

}

export default AddressPage;
