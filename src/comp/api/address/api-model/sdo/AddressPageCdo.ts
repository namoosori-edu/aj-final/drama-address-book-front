import { Address } from '../vo';


class AddressPageCdo {
  name: string;

  address: Address;

  phoneNumber: string;

  addressBookId: string;

  constructor(name: string, address: Address, phoneNumber: string, addressBookId: string) {
    this.name = name;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.addressBookId = addressBookId;
  }

  static new(): AddressPageCdo {
    return new AddressPageCdo('', Address.new(), '', '');
  }

}

export default AddressPageCdo;
