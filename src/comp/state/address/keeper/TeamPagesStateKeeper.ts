import {AddressPage, FindTeamPagesPagedQuery, FindTeamPagesQuery, TeamBookSeekApiStub} from "../../../api";
import {makeAutoObservable, runInAction} from "mobx";
import {Offset} from "@nara/accent";

class TeamPagesStateKeeper {
  static _instance: TeamPagesStateKeeper;

  private readonly teamBookSeekApiStub: TeamBookSeekApiStub;

  addressPages: AddressPage[] = [];

  offset: Offset = Offset.newDescending(0,12,'registrationTime');

  totalCount = 0;

  addressBookId = '';

  static get instance() {
    if (!TeamPagesStateKeeper._instance) {
      TeamPagesStateKeeper._instance = new TeamPagesStateKeeper();
    }
    return TeamPagesStateKeeper._instance;
  }

  constructor(
    teamBookSeekApiStub: TeamBookSeekApiStub = TeamBookSeekApiStub.instance,
  ) {
    this.teamBookSeekApiStub = teamBookSeekApiStub;
    makeAutoObservable(this, {}, { autoBind: true });
  }

  async findTeamAddressPages(addressBookId: string): Promise<AddressPage[]> {
    //
    const query = FindTeamPagesPagedQuery.byQuery(addressBookId, this.offset);
    const offsetElementList = await this.teamBookSeekApiStub.findTeamPagesPaged(query);

    runInAction( () => {
      this.addressPages = offsetElementList.results;
      this.totalCount = offsetElementList.totalCount;
    });

    return this.addressPages;
  }
}

export default TeamPagesStateKeeper;
