import {
  Address,
  AddressPage,
  AddressPageCdo,
  Field,
  FindTeamPageQuery,
  TeamBookFlowApiStub,
  TeamBookSeekApiStub
} from "../../../api";
import {makeAutoObservable, runInAction} from "mobx";
import {CommandResponse, NameValue, NameValueList, NotInstantiatedException} from "@nara/accent";
import _ from 'lodash';
import field from "../../../api/address/api-model/vo/Field";
import {ModifyTeamPageCommand, RegisterTeamPageCommand} from "../../../api/teambook/command/command";


class TeamPageStateKeeper {
  static _instance: TeamPageStateKeeper;

  private readonly teamBookFlowApiStub: TeamBookFlowApiStub;

  private readonly teamBookSeekApiStub: TeamBookSeekApiStub;

  addressPage: AddressPage | null = null;

  static get instance() {
    if (!TeamPageStateKeeper._instance) {
      TeamPageStateKeeper._instance = new TeamPageStateKeeper();
    }
    return TeamPageStateKeeper._instance;
  }

  constructor(
    teamBookFlowApiStub: TeamBookFlowApiStub = TeamBookFlowApiStub.instance,
    teamBookSeekApiStub: TeamBookSeekApiStub = TeamBookSeekApiStub.instance,
  ) {
    this.teamBookFlowApiStub = teamBookFlowApiStub;
    this.teamBookSeekApiStub = teamBookSeekApiStub;
    makeAutoObservable(this, {}, { autoBind:true });
  }

  init(addressBookId: string) {
    this.addressPage = AddressPage.new(addressBookId);
  }

  setAddressPageProp(name: string, value: any) {
    if(!this.addressPage) {
      throw new NotInstantiatedException('TeamPageStateKeeper.setAddressPageProp', 'addressPage is null');
    }
    this.addressPage = Object.assign(AddressPage.new(), _.set(this.addressPage, name, value));
  }

  setAddressPageField(targetIndex: number, name: string, value: any) {
    if (!this.addressPage) {
      throw new NotInstantiatedException('TeamPageStateKeeper.setAddressPageField', 'addressPage is null');
    }

    const fields = this.addressPage.fields.map((field, index) =>
      targetIndex === index
      ? Object.assign(Field.new(), _.set(field, name, value))
      : field,
      );

      this.setAddressPageProp('fields', fields);
  }

  addField() {
    //
    if(!this.addressPage) {
      throw new NotInstantiatedException('TeamPageStateKeeper.addField', 'addressPage is null');
    }
    this.setAddressPageProp('fields', [...this.addressPage.fields, Field.new()]);
  }

  removeField(targetIndex: number) {
    //
    if (!this.addressPage) {
      throw new NotInstantiatedException('TeamPageStateKeeper.removeField', 'addressPage is null');
    }
    this.setAddressPageProp('fields', this.addressPage.fields.filter((field, index) => targetIndex !== index));
  }

  async register(addressPage: AddressPage): Promise<CommandResponse> {
    //
    const addressPageCdo = AddressPageCdo.new();
    addressPageCdo.addressBookId = addressPage.addressBookId;
    addressPageCdo.name = addressPage.name;
    addressPageCdo.phoneNumber = addressPage.phoneNumber;
    addressPageCdo.address = addressPage.address || Address.new();

    const command = RegisterTeamPageCommand.new(addressPageCdo);

    return this.teamBookFlowApiStub.registerTeamPage(command);
  }

  async modify(addressPageId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = ModifyTeamPageCommand.new(addressPageId, nameValues);

    return this.teamBookFlowApiStub.modifyTeamAddress(command);
  }

  async findTeamPageById(addressPageId: string): Promise<AddressPage> {
    //
    const addressPageQuery = FindTeamPageQuery.byQuery(addressPageId);
    const addressPage = await this.teamBookSeekApiStub.findTeamPage(addressPageQuery);

    runInAction(() => this.addressPage = addressPage);
    return addressPage;
  }

  async toggleTeamBaseAddress(addressPage: AddressPage) {
    //
    if (addressPage.baseAddress) {
      return this.modify(addressPage.id, new NameValueList(new NameValue('baseAddress', 'false')));
    }
  }




}
export default TeamPageStateKeeper;
