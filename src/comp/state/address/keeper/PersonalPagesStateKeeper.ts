import { NotInstantiatedException, Offset } from '@nara/accent';
import _ from 'lodash';
import { makeAutoObservable, runInAction } from 'mobx';
import { AddressPage, PersonalBookSeekApiStub } from '../../../api';
import FindPersonalPagesPagedQuery from '../../../api/personalbook/query/query/FindPersonalPagesPagedQuery';


class PersonalPagesStateKeeper {
  static _instance: PersonalPagesStateKeeper;

  private readonly personalBookSeekApiStub: PersonalBookSeekApiStub;

  addressPages: AddressPage[] = [];

  offset: Offset = Offset.newAscending(0, 12, 'registrationTime');

  totalCount = 0;

  addressBookId = '';

  static get instance() {
    if (!PersonalPagesStateKeeper._instance) {
      PersonalPagesStateKeeper._instance = new PersonalPagesStateKeeper();
    }
    return PersonalPagesStateKeeper._instance;
  }

  constructor(
    personalBookSeekApiStub: PersonalBookSeekApiStub = PersonalBookSeekApiStub.instance,
  ) {
    this.personalBookSeekApiStub = personalBookSeekApiStub;
    makeAutoObservable(this, {}, { autoBind: true });
  }

  setAddressPageProp(index: number, name: keyof AddressPage, value: any) {
    if (!this.addressPages || !this.addressPages[index]) {
      throw new NotInstantiatedException('PersonalPagesStateKeeper.setAddressPageProp', `addressPages[${index}] is null`);
    }
    this.addressPages[index] = _.set(this.addressPages[index], name, value);
  }

  setOffsetProp(name: keyof Offset, value: any) {
    this.offset = Object.assign(new Offset(), _.set(this.offset, name, value));
  }

  setAddressBookId(addressBookId: string) {
    //
    this.addressBookId = addressBookId;
  }

  changePage(page: number) {
    //
    const { limit } = this.offset;
    this.offset = Object.assign(new Offset(), _.set(this.offset, 'offset', limit * (page - 1)));
  }

  clear() {
    this.addressPages = [];
    this.offset = Offset.newAscending(0, 12, 'registrationTime');
    this.totalCount = 0;
  }

  async findPersonalAddressPages(addressBookId: string): Promise<AddressPage[]> {
    //
    const query = FindPersonalPagesPagedQuery.byQuery(addressBookId, this.offset);
    const offsetElementList = await this.personalBookSeekApiStub.findPersonalPagesPaged(query);

    runInAction(() => {
      this.addressPages = offsetElementList.results;
      this.totalCount = offsetElementList.totalCount;
    });

    return this.addressPages;
  }
}

export default PersonalPagesStateKeeper;
