import { CommandResponse, NameValueList, NotInstantiatedException } from '@nara/accent';
import _ from 'lodash';
import { makeAutoObservable, runInAction } from 'mobx';
import {
  AddressBook,
  AddressBookCdo,
  FindPersonalBookQuery,
  ModifyPersonalBookCommand,
  PersonalBookFlowApiStub,
  PersonalBookSeekApiStub,
  RegisterPersonalBookCommand,
} from '../../../api';


class PersonalBookStateKeeper {
  static _instance: PersonalBookStateKeeper;

  private readonly personalBookFlowApiStub: PersonalBookFlowApiStub;

  private readonly personalBookSeekApiStub: PersonalBookSeekApiStub;

  addressBook: AddressBook | null = null;

  static get instance() {
    if (!PersonalBookStateKeeper._instance) {
      PersonalBookStateKeeper._instance = new PersonalBookStateKeeper();
    }
    return PersonalBookStateKeeper._instance;
  }

  constructor(
    personalBookFlowApiStub: PersonalBookFlowApiStub = PersonalBookFlowApiStub.instance,
    personalBookSeekApiStub: PersonalBookSeekApiStub = PersonalBookSeekApiStub.instance,
  ) {
    this.personalBookFlowApiStub = personalBookFlowApiStub;
    this.personalBookSeekApiStub = personalBookSeekApiStub;
    makeAutoObservable(this, {}, { autoBind:true });
  }

  init() {
    this.addressBook = AddressBook.new();
  }

  setAddressBookProp(name: string, value: any) {
    if (!this.addressBook) {
      throw new NotInstantiatedException('PersonalBookStateKeeper.setAddressBookProp', 'addressBook is null');
    }
    this.addressBook = _.set(this.addressBook, name, value);
  }

  clear() {
    this.addressBook = null;
  }

  async register(addressBookCdo: AddressBookCdo): Promise<CommandResponse> {
    const command = RegisterPersonalBookCommand.new(addressBookCdo);
    return this.personalBookFlowApiStub.registerPersonalAddress(command);
  }

  async modify(addressBookId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = ModifyPersonalBookCommand.new(addressBookId, nameValues);
    return this.personalBookFlowApiStub.modifyPersonalAddress(command);
  }

  async findPersonalAddressBookById(addressBookId: string): Promise<AddressBook> {
    const addressBookQuery = FindPersonalBookQuery.byQuery(addressBookId);
    const addressBook = await this.personalBookSeekApiStub.findPersonalBook(addressBookQuery);

    runInAction(() => this.addressBook = addressBook);
    return addressBook;
  }
}

export default PersonalBookStateKeeper;
