import { CommandResponse, NameValue, NameValueList, NotInstantiatedException } from '@nara/accent';
import { makeAutoObservable, runInAction } from 'mobx';
import _ from 'lodash';
import {
  Address,
  AddressPage,
  AddressPageCdo,
  AssignPersonalBaseAddressCommand,
  Field,
  FindPersonalPageQuery,
  MigratePersonalPageCommand,
  ModifyPersonalPageCommand,
  PersonalBookFlowApiStub,
  PersonalBookSeekApiStub,
  RegisterPersonalPageCommand,
} from '../../../api';


class PersonalPageStateKeeper {
  static _instance: PersonalPageStateKeeper;

  private readonly personalBookFlowApiStub: PersonalBookFlowApiStub;

  private readonly personalBookSeekApiStub: PersonalBookSeekApiStub;

  addressPage: AddressPage | null = null;

  static get instance() {
    if (!PersonalPageStateKeeper._instance) {
      PersonalPageStateKeeper._instance = new PersonalPageStateKeeper();
    }
    return PersonalPageStateKeeper._instance;
  }

  constructor(
    personalBookFlowApiStub: PersonalBookFlowApiStub = PersonalBookFlowApiStub.instance,
    personalBookSeekApiStub: PersonalBookSeekApiStub = PersonalBookSeekApiStub.instance,
  ) {
    this.personalBookFlowApiStub = personalBookFlowApiStub;
    this.personalBookSeekApiStub = personalBookSeekApiStub;
    makeAutoObservable(this, {}, { autoBind:true });
  }

  init(addressBookId: string) {
    this.addressPage = AddressPage.new(addressBookId);
  }

  setAddressPageProp(name: string, value: any) {
    if (!this.addressPage) {
      throw new NotInstantiatedException('PersonalPageStateKeeper.setAddressPageProp', 'addressPage is null');
    }
    this.addressPage = Object.assign(AddressPage.new(), _.set(this.addressPage, name, value));
  }

  setAddressPageField(targetIndex: number, name: string, value: any) {
    if (!this.addressPage) {
      throw new NotInstantiatedException('PersonalPageStateKeeper.setAddressPageField', 'addressPage is null');
    }

    const fields = this.addressPage.fields.map((field, index) =>
      targetIndex === index
        ? Object.assign(Field.new(), _.set(field, name, value))
        : field,
    );

    this.setAddressPageProp('fields', fields);
  }

  addField() {
    //
    if (!this.addressPage) {
      throw new NotInstantiatedException('PersonalPageStateKeeper.addField', 'addressPage is null');
    }
    this.setAddressPageProp('fields', [...this.addressPage.fields, Field.new()]);
  }

  removeField(targetIndex: number) {
    //
    if (!this.addressPage) {
      throw new NotInstantiatedException('PersonalPageStateKeeper.removeField', 'addressPage is null');
    }
    this.setAddressPageProp('fields', this.addressPage.fields.filter((field, index) => targetIndex !== index));
  }

  clear() {
    this.addressPage = null;
  }

  async register(addressPage: AddressPage): Promise<CommandResponse> {
    const addressPageCdo = AddressPageCdo.new();
    addressPageCdo.addressBookId = addressPage.addressBookId;
    addressPageCdo.name = addressPage.name;
    addressPageCdo.phoneNumber = addressPage.phoneNumber;
    addressPageCdo.address = addressPage.address || Address.new();

    const command = RegisterPersonalPageCommand.new(addressPageCdo);

    return this.personalBookFlowApiStub.registerPersonalPage(command);
  }

  async modify(addressPageId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = ModifyPersonalPageCommand.new(addressPageId, nameValues);

    return this.personalBookFlowApiStub.modifyPersonalPage(command);
  }

  async findPersonalPageById(addressPageId: string): Promise<AddressPage> {
    //
    const addressPageQuery = FindPersonalPageQuery.byQuery(addressPageId);
    const addressPage = await this.personalBookSeekApiStub.findPersonalPage(addressPageQuery);

    runInAction(() => this.addressPage = addressPage);
    return addressPage;
  }

  async togglePersonalBaseAddress(addressPage: AddressPage) {
    //
    if (addressPage.baseAddress) {
      return this.modify(addressPage.id, new NameValueList(new NameValue('baseAddress', 'false')));
    } else {
      return this.assignPersonalBaseAddress(addressPage.addressBookId, addressPage.id);
    }
  }

  async assignPersonalBaseAddress(addressBookId: string, addressPageId: string): Promise<CommandResponse> {
    //
    const command = AssignPersonalBaseAddressCommand.new(addressBookId, addressPageId);

    return this.personalBookFlowApiStub.assignPersonalBaseAddress(command);
  }

  async migratePersonalPage(addressPageId: string, teamAddressBookId: string): Promise<CommandResponse> {
    const command = MigratePersonalPageCommand.new(addressPageId, teamAddressBookId);

    return this.personalBookFlowApiStub.migratePersonalPage(command);
  }
}

export default PersonalPageStateKeeper;
