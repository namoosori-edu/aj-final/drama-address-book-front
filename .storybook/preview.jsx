import * as React from 'react';
import { configure, isObservableArray } from 'mobx';
import { AppContext, axiosApi, dialogUtil } from '@nara/prologue';
import { default as DialogView } from './config/dialog';
import {AuthProvider, DockProvider} from "@nara/dock";
import { devCredential, devDock } from './config/devconfig';

configure({
  useProxies: 'ifavailable',
  isolateGlobalState: true,
  computedRequiresReaction: true,
  reactionRequiresObservable: true,
});

(() => {
  const isArray = Array.isArray;
  Object.defineProperty(Array, 'isArray', {
    value: (target) => (isObservableArray(target) || isArray(target)),
  });
})();

export const parameters = {
  actions: {argTypesRegex: '^on[A-Z].*'},
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

// axiosApi.interceptors.request.use((request) => {
//   request.headers.Authorization = `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbklkIjoiYWRtaW5AbmV4dHJlZS5pbyIsInBhdmlsaW9uSWQiOiIxOjE6MSIsInVzZXJfbmFtZSI6ImFkbWluQG5leHRyZWUuaW8iLCJkaXNwbGF5TmFtZSI6Iu2ZjSDrgpjrnbwiLCJzY29wZSI6WyJjaXRpemVuIl0sImNpdGl6ZW5Vc2VySWQiOiJjNmI4ZDUzYi0xMTAyLTRhM2MtOGVkNi0zNDcyMTM5ODNiMDIiLCJjaXRpemVuU2Vzc2lvbklkIjoiZmNmOWUwOWMtYjFiYy00N2ZkLTllN2MtNWNkODJkNzA1MGJhIiwiZXhwIjoxNjU2NDE4NTcwLCJqdGkiOiItc2d5eEJkSTJTclptRXUwWGJwdE1IeHN6UkUiLCJlbWFpbCI6ImFkbWluQG5leHRyZWUuaW8iLCJjaW5lcm9vbUlkcyI6WyIxOjE6MToxIiwiMToxOjE6MiIsIjE6MToxOjMiXSwiY2xpZW50X2lkIjoibmFyYSJ9.pcHGORfhz5hMjBo8ZiH-X25jrDYhdLxGp2jmWvapDwY`;
//   request.headers.actorId = `1@1:1:1:1-1`;
//   request.headers.roles = `owner, director`;
//   return request;
// });

export const decorators = [
  Story => {
    //
    return (
      <div>
        <AppContext.Provider>
          <AuthProvider development devCredential={devCredential}>
            <DockProvider development devDock={devDock}>
              <dialogUtil.Viewer renderDialog={(params) => (<DialogView {...params} />)}/>
              <Story/>
            </DockProvider>
          </AuthProvider>
        </AppContext.Provider>
      </div>
    );
  },
];
