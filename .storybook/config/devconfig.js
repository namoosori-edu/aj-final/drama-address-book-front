export const devCredential = {
  username: 'admin@nextree.io',
  password: '1234',
  pavilionId: '1:1:1',
};

const availableDock = {
  citizen: {
    id: '1@1:1:1', name: '홍나라'
  }, pavilion: {
    id: '1:1:1', name: '나라웨이'
  }, cinerooms: [{
    audience: {
      id: '1@1:1:1:3', name: '홍나라'
    }, cineroom: {
      id: '1:1:1:3', name: '타운관리'
    }, current: false, stages: [{
      actor: {
        id: '1@1:1:1:3-1', name: '홍나라'
      }, stage: {
        id: '1:1:1:3-1', name: '타운'
      }, kollections: [{
        kollection: {
          id: 'contact_1.0.0', name: 'Contact'
        }, path: 'contact', current: false, kollecties: [{
          path: 'address-book', name: '주소록', requiredRoles: ['owner']
        },], stageRoles: [{
          stageId: '1:1:1:3-1',
          kollectionVersionId: 'contact_1.0.0',
          code: 'director',
          name: '관리자',
          dramaRoles: [{
            code: 'director',
            name: '관리자',
            defaultRole: false,
            dramaId: 'address-book'
          }, {
            code: 'owner',
            name: '소유자',
            defaultRole: true,
            dramaId: 'address-book'
          }],
          defaultRole: false,
        }, {
          stageId: '1:1:1:3-1',
          kollectionVersionId: 'contact_1.0.0',
          code: 'owner',
          name: '소유자',
          dramaRoles: [{
            code: 'owner',
            name: '소유자',
            defaultRole: true,
            dramaId: 'address-book'
          }],
          defaultRole: true,
        }]
      }]
    }]
  }]
};

export const devDock = {
  availableDock,
  currentCitizen: availableDock.citizen,
  currentPavilion: availableDock.pavilion,
  currentAudience: availableDock.cinerooms[0].audience,
  currentCineroom: availableDock.cinerooms[0].cineroom,
  currentActor: availableDock.cinerooms[0].stages[0].actor,
  currentStage: {
    ...availableDock.cinerooms[0].stages[0].stage,
    kollections: availableDock.cinerooms[0].stages[0].kollections
  },
  currentKollection: availableDock.cinerooms[0].stages[0].kollections[0].kollection,
  currentStageRoles: ['director', 'owner'],
  currentDramaRoles: ['address-book:director', 'address-book:owner'],
  currentDramaRoleMap: {
    'address-book': ['director', 'owner']
  }
};

export const development = process.env.NODE_ENV === 'development';
// export const development = false;
